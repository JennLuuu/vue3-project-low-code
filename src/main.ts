/*
 * @Author: JennLuuu 1129694837@qq.com
 * @Date: 2024-07-05 09:02:25
 * @LastEditors: JennLuuu
 * @LastEditTime: 2024-07-05 09:31:38
 * @FilePath: \vue3-project-LowCode\src\main.ts
 * @Description:
 */
import { createApp } from "vue";
import App from "./App.vue";
import 'normalize.css'

// 引入 element-plus
import ElementPlus from "element-plus"
import "element-plus/dist/index.css"

// 引入路由
import router from "@/router/index";

// 引入 pinia
import store from "@/store/index"

const app = createApp(App)

// 使用
app.use(router)
app.use(ElementPlus)
app.use(store)

app.mount("#app");
