/*
 * @Author: JennLuuu 1129694837@qq.com
 * @Date: 2024-07-05 09:18:35
 * @LastEditors: JennLuuu
 * @LastEditTime: 2024-07-05 09:19:38
 * @FilePath: \vue3-project-LowCode\src\router\index.ts
 * @Description: 路由配置
 */
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("@/views/index.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
