import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from "path"; //类型报错，pnpm add @types/node -D

const resolve = (relativePath: string)=>path.resolve(__dirname,relativePath)

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // 配置路径别名
  resolve: {
    alias: {
      "@": resolve("src")
    }
  }
})
